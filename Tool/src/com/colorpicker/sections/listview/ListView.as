package com.colorpicker.sections.listview {
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	import com.soulwire.ASEEncoder;
	import flash.text.TextFormat;
	import flash.text.GridFitType;
	import flash.text.TextFieldAutoSize;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.bit101.components.PushButton;
	import com.bit101.components.Window;
	import flash.display.Sprite;

	/**
	 * @author michael
	 */
	public class ListView extends Sprite {
		
		private var bg:Sprite;
		private var _list:Vector.<String>;
		private var displayWindow:Window;
		private var rawDataAS3Btn:PushButton;
		private var hypeBtn:PushButton;
		private var rawDataProcessingBtn:PushButton;
		private var processingBtn:PushButton;
		private var saveAsASE:PushButton;
		private var copyDataToClipBoard:PushButton;
		private var textArea:TextField;
		
		public function ListView() {
			
			bg = new Sprite();
			with(bg.graphics){
				beginFill(0x000000, 0.5);
				drawRect(0, 0, 1024, 728);
				endFill();
			}
			addChild(bg);
			bg.addEventListener(MouseEvent.CLICK, onBGCLick);
			
			displayWindow = new Window();
			displayWindow.setSize(800, 600);
			displayWindow.draggable = false;
			displayWindow.move((1024 - 800) / 2, (728 - 600) / 2);
			displayWindow.title = "Color List";
			addChild(displayWindow);
			
			rawDataAS3Btn = new PushButton();
			rawDataAS3Btn.label = "View Raw List";
			rawDataAS3Btn.x = 10;
			rawDataAS3Btn.y = 10;
			rawDataAS3Btn.setSize(160, 20);
			rawDataAS3Btn.addEventListener(MouseEvent.CLICK, onRawDataAS3BtnPress);
			displayWindow.addChild(rawDataAS3Btn);
			
			hypeBtn = new PushButton();
			hypeBtn.label = "View For AS3 Hype";
			hypeBtn.x = 10 + rawDataAS3Btn.width + rawDataAS3Btn.x;
			hypeBtn.y = 10;
			hypeBtn.setSize(160, 20);
			hypeBtn.addEventListener(MouseEvent.CLICK, onAS3HypeBtnPress);
			displayWindow.addChild(hypeBtn);
			
			rawDataProcessingBtn = new PushButton();
			rawDataProcessingBtn.label = "View For Processing";
			rawDataProcessingBtn.x = 10 + hypeBtn.width + hypeBtn.x;
			rawDataProcessingBtn.y = 10;
			rawDataProcessingBtn.setSize(160, 20);
			rawDataProcessingBtn.addEventListener(MouseEvent.CLICK, onRawDataProcessingBtnPress);
			displayWindow.addChild(rawDataProcessingBtn);
			
			processingBtn = new PushButton();
			processingBtn.label = "View For Processing Hype";
			processingBtn.x = 10 + rawDataProcessingBtn.width + rawDataProcessingBtn.x;
			processingBtn.y = 10;
			processingBtn.setSize(160, 20);
			processingBtn.addEventListener(MouseEvent.CLICK, onProcessingHypeBtnPress);
			displayWindow.addChild(processingBtn);
			
			copyDataToClipBoard = new PushButton();
			copyDataToClipBoard.label = "Copy Data To Clip Board";
			copyDataToClipBoard.x = 10;
			copyDataToClipBoard.y = 600 - copyDataToClipBoard.height - 30;
			copyDataToClipBoard.setSize(160, 20);
			copyDataToClipBoard.addEventListener(MouseEvent.CLICK, onCopyData);
			displayWindow.addChild(copyDataToClipBoard);
			
			saveAsASE = new PushButton();
			saveAsASE.label = "Save As ASE";
			saveAsASE.x = 10 + copyDataToClipBoard.width + copyDataToClipBoard.x;
			saveAsASE.y = copyDataToClipBoard.y;
			saveAsASE.setSize(160, 20);
			saveAsASE.addEventListener(MouseEvent.CLICK, onSaveAsASEPress);
			displayWindow.addChild(saveAsASE);
			
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = 15;
			
			textArea = new TextField();
			textArea.selectable = true;
			textArea.antiAliasType = AntiAliasType.NORMAL;
			textArea.x = 10;
			textArea.y = 40;
			textArea.width = 780;
			textArea.height = 450;
			textArea.wordWrap = true;
			textArea.defaultTextFormat = textFormat;
			textArea.gridFitType = GridFitType.PIXEL;
			displayWindow.addChild(textArea);
			
		}

		private function onBGCLick(event : MouseEvent):void {
			dispatchEvent(new Event("bgclick", true, false));
		}
		
		public function set list(newList:Vector.<String>):void{
			_list = newList;
		}
		
		public function onRawDataAS3BtnPress(e:MouseEvent):void{
			var cleanList:Vector.<String> = createCleanListUintFormat();
			textArea.text = cleanList.toString();
		}
		
		public function onAS3HypeBtnPress(e:MouseEvent):void{
			var cleanList:Vector.<String> = createCleanListUintFormat();
			var textValue:String = "var color:ColorPool = new ColorPool(";
			textValue += cleanList.toString();
			textValue +=  ");";
			textArea.text = textValue;
		}
		
		public function onRawDataProcessingBtnPress(e:MouseEvent):void{
			var cleanList:Vector.<String> = createCleanListAsHEXFormat()
			textArea.text = cleanList.toString();
		}
		
		public function onProcessingHypeBtnPress(e:MouseEvent):void{
			var cleanList:Vector.<String> = createCleanListAsHEXFormat();
			var textValue:String = "color[]palette = {";
			textValue += cleanList.toString();
			textValue +=  "};";
			textArea.text = textValue;
		}
		
		public function onSaveAsASEPress(e:MouseEvent):void{
		  var aseEncoder:ASEEncoder = new ASEEncoder(_list);
		  var aseFile:ByteArray = aseEncoder.encode();
		  var fileRef:FileReference = new FileReference();
		  fileRef.save(aseFile, "ColorToolSwatch.ase");
		}
		
		public function onCopyData(e:MouseEvent):void{
		  if(textArea.text != "") Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, textArea.text);
		}
		
		public function createCleanListUintFormat():Vector.<String>{
		  var cleanList:Vector.<String> = new Vector.<String>();
			for each(var s:String in _list){
				cleanList.push(s.toUpperCase().replace("X", "x"));
			}
			return cleanList;
		}
		
		public function createCleanListAsHEXFormat():Vector.<String>{
		  var cleanList:Vector.<String> = new Vector.<String>();
			for each(var s:String in _list){
				cleanList.push(s.toUpperCase().replace("0X", "#"));
			}
			return cleanList;
		}
		
		
	}
}
