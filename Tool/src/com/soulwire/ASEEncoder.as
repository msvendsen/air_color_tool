package com.soulwire{
  import flash.utils.ByteArray;
  
  public class ASEEncoder{
    
    private var colors:Array;
    
    private const FILE_SIGNATURE:String = "ASEF";
    
    public function ASEEncoder(colorsToUse:Vector.<String>){
      colors = new Array();
      for each(var s:String in colorsToUse){
        colors.push(parseInt(s));
      }
    }
    
    public function encode():ByteArray{
      
      var swatch:ByteArray = new ByteArray();
      var ase:ByteArray = new ByteArray();
      var hex:String;
      var pix:uint;
      
      ase.writeUTFBytes(FILE_SIGNATURE); //header
      ase.writeInt(0x10000); //version
      ase.writeInt(colors.length * 2); //blocks
      
      var i:int = 0;
      var max:int = colors.length;
      for(i; i<max; ++i){
        
        pix = colors[i];
        swatch.length = 0;
        
        //start of group
        
        ase.writeShort(0xC001);
        ase.writeInt(0);
        ase.writeShort(1);
        
        //swatch name
        hex = pix.toString(16);
        while(hex.length < 6) hex = "0" + hex;
        
        swatch.writeShort((hex = "#" + hex).length + 1);
        for(var n : int = 0; n < hex.length; ++n) swatch.writeShort(hex.charCodeAt(n));
        swatch.writeShort(0);
        
        //colors
        swatch.writeUTFBytes ("RGB ");
        swatch.writeFloat((pix >> 16 & 0xFF) / 255);
        swatch.writeFloat((pix >> 8 & 0xFF) / 255);
        swatch.writeFloat((pix & 0xFF) / 255);
        swatch.writeShort(2);
        
        //write swatch
        ase.writeInt(swatch.length);
        ase.writeBytes(swatch);
      }
      return ase;
    }
    
  }
  
}